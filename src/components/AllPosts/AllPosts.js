import React, {useEffect, useState} from 'react';
import axios from "axios";
import './AllPosts.css';
import {NavLink} from "react-router-dom";

const AllPosts = () => {
    const [quotes, setQuotes] = useState([]);
    const [id, setId] = useState([]);

    useEffect(() => {
        const axiosData = async () => {
            const response = await axios.get('https://js-11-cw-8-default-rtdb.firebaseio.com/quotes.json');
            setId(Object.keys(response.data));
            setQuotes(Object.values(response.data))
        };
        axiosData().catch(console.log);
    }, []);

    const deleteQuote = index => {
        const axiosData = async () => {
            await axios.delete(`https://js-11-cw-8-default-rtdb.firebaseio.com/quotes/${index}.json`);
        };
        axiosData().catch(console.log);
    };

    //из firebase цитата удаляется, а на странице нет, так и не понял какую зависимость нужно передавть для useEffect

    return (
        <>
            {quotes.map((quote, i) => (
                <div key={i}>
                    <p className="Category">{quote.category}</p>
                    <div className="QuotesWrapper">
                        <blockquote>
                            "{quote.text}"
                            <p>&mdash; {quote.name}</p>
                        </blockquote>
                        <NavLink to={`/quotes/${id[i]}/edit`} className="EditBtn">
                            <button type="button">Edit</button>
                        </NavLink>
                        <button type="button" className="DeleteBtn" onClick={() => deleteQuote(id[i])}>Delete</button>
                    </div>
                </div>
            ))}
        </>
    );
};

export default AllPosts;
