import React, {useEffect, useState} from 'react';
import axios from "axios";
import './QuotesCategory.css';

const QuotesCategory = ({match, history}) => {
    const [quotesCategory, setQuotesCategory] = useState([]);
    const [id, setId] = useState([]);
    const category = match.params.humour;

    useEffect(() => {
        const axiosData = async () => {
            const response = await axios.get(`https://js-11-cw-8-default-rtdb.firebaseio.com/quotes.json?orderBy=%22category%22&equalTo=%22${category}%22`);
            setId(Object.keys(response.data));
            setQuotesCategory(Object.values(response.data));
        };
        axiosData().catch(console.log);
    }, [category]);

    const deleteQuote = index => {
        const axiosData = async () => {
            await axios.delete(`https://js-11-cw-8-default-rtdb.firebaseio.com/quotes/${index}.json`);
            history.push('/');
        };
        axiosData().catch(console.log);
    };

    return (
        <div>
            <p className="Category">{category}</p>
            {quotesCategory.map((quote, i) => (
                <div key={i} className="QuotesWrapper">
                    <p>{quote.text}</p>
                    <p>{quote.name}</p>
                    <button onClick={() => deleteQuote(id[i])}>Delete</button>
                </div>
            ))}
        </div>
    );
};

export default QuotesCategory;