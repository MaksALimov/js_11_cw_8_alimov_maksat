import React, {useEffect, useState} from 'react';
import axios from "axios";
import './Edit.css'

const Edit = ({match}) => {
    const [prevPost, setPrevPost] = useState({
        category: '',
        name: '',
        text: ''
    });

    useEffect(() => {
        const axiosData = async () => {
            const response = await axios.get(`https://js-11-cw-8-default-rtdb.firebaseio.com/quotes/${match.params.id}.json`);
            setPrevPost({
                category: response.data.category,
                name: response.data.name,
                text: response.data.text,
            });
        };
        axiosData().catch(console.log);
    }, [match.params.id]);

    const onInputChange = e => {
        const {name, value} = e.target;

        setPrevPost(prevState => ({
            ...prevState,
            [name]: value
        }))
    };

    const editQuote = e => {
        e.preventDefault();

        const axiosData = async () => {
            await axios.put(`https://js-11-cw-8-default-rtdb.firebaseio.com/quotes/${match.params.id}.json`, {
                category: prevPost.category,
                name: prevPost.name,
                text: prevPost.text
            })

            setPrevPost({
                category: '',
                name: '',
                text: '',
            });

        };
        axiosData().catch(console.log);
    };

    return (
        <div>
            <form onSubmit={editQuote} className="EditWrapper">
                <span>Edit a quote</span>
                <p>Category</p>
                <select name="category" value={prevPost.category} onChange={onInputChange}>
                    <option value="star-wars">star-wars</option>
                    <option value="saying">saying</option>
                    <option value="famousPeople">famousPeople</option>
                    <option value="humour">humour</option>
                    <option value="motivational">motivational</option>
                </select>
                <label>
                    Author
                    <input
                        type="text"
                        value={prevPost.name}
                        name="name"
                        onChange={onInputChange}
                    />
                </label>
                <label>
                    Quote Text
                    <textarea
                        cols="30"
                        rows="10"
                        value={prevPost.text}
                        name="text"
                        onChange={onInputChange}
                    />
                </label>
                <button type="submit">Save</button>
            </form>
        </div>
    );
};

export default Edit;