import React, {useState} from 'react';
import axios from "axios";
import './SubmitNewQuote.css';

const SubmitNewQuote = () => {
    const [newQuote, setNewQuote] = useState({
        category: '',
        name: '',
        text: '',
    });

    const onInputChange = e => {
        const {name, value} = e.target;

        setNewQuote(prevState => ({
            ...prevState,
            [name]: value
        }))
    };

    const sendQuote = async e => {
        e.preventDefault();

        try {
            await axios.post('https://js-11-cw-8-default-rtdb.firebaseio.com/quotes.json', {
                category: newQuote.category,
                name: newQuote.name,
                text: newQuote.text,
            });
        } catch (e) {
            console.log(e);
        }
        setNewQuote({
            name: '',
            text: '',
        });
    };

    return (
        <div>
            <p className="SubmitQuoteMsg">Submit new Quote</p>
            <p className="SubmitQuoteCategory">Category</p>
            <form onSubmit={sendQuote} className="SubmitQuoteForm">
                <select value={newQuote.category} name="category" onChange={onInputChange}>
                    <option value="star-wars">star-wars</option>
                    <option value="saying">saying</option>
                    <option value="famousPeople">famousPeople</option>
                    <option value="humour">humour</option>
                    <option value="motivational">motivational</option>
                </select>
                <label className="author">
                    Author
                    <input
                        type="text"
                        value={newQuote.name}
                        name="name"
                        onChange={onInputChange}
                    />
                </label>
                <label>
                    Quote text
                    <textarea
                        cols="30"
                        rows="10"
                        value={newQuote.text}
                        name="text"
                        onChange={onInputChange}
                    />
                </label>
                <button type="submit">Save</button>
            </form>
        </div>
    );
};

export default SubmitNewQuote;