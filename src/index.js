import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {BrowserRouter, NavLink, Route, Switch} from "react-router-dom";
import SubmitNewQuote from "./containers/SubmitNewQuote/SubmitNewQuote";
import {categories} from "./config";
import QuotesCategory from "./components/QuotesCategory/QuotesCategory";
import Edit from "./containers/Edit/Edit";

ReactDOM.render(
    <BrowserRouter>
        <NavLink to="/">
            Quotes central
        </NavLink>
        <br/>
        <NavLink to="/">
            Quotes
        </NavLink>
        <br/>
        <NavLink to="/add-quote">
            Submit New Quote
        </NavLink>
        {categories.map((category, i) => (
            <NavLink key={i} to={`/quotes/${category.id}`}>
                {category.title}
            </NavLink>
        ))}
        <Switch>
            <Route path="/" exact component={App}/>
            <Route path="/quotes/:humour" exact component={QuotesCategory}/>
            <Route path="/quotes/:motivational" exact component={QuotesCategory}/>
            <Route path="/quotes/:saying" exact component={QuotesCategory}/>
            <Route path="/quotes/:famousPeople" exact component={QuotesCategory}/>
            <Route path="/quotes/:star-wars" exact component={QuotesCategory}/>
            <Route path="/quotes/:id/edit" component={Edit}/>
            <Route path="/add-quote" component={SubmitNewQuote}/>
        </Switch>
    </BrowserRouter>,
  document.getElementById('root')
);
